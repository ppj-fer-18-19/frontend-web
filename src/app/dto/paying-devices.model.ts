export class PayingDevices {

    constructor(public id: string,
        public info: string,
        public latitude: number,
        public longitude: number,
        public parkingId: string) {}

}