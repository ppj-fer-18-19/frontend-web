export class Sensor {
    public id:	string;
    public lastUpdated:	string;
    public latitude:	number;
    public longitude:	number;
    public occupied:	boolean;
    public parkingId:	string;

    constructor(id:	string, lastUpdated:	string, latitude:	number, longitude:	number, occupied:	boolean, parkingId:	string){
        this.id = id;
        this.lastUpdated = lastUpdated;
        this.latitude = latitude;
        this.longitude = longitude;
        this.occupied = occupied;
        this.parkingId = parkingId;
    }
}