import { Parking } from "./parking.model";
import { User } from "./user.model";



export interface Content {
    enabled: boolean;
    id: string;
    maxSpaces: number;
    name: string;
    occupiedSpaces: number;
    zoneId: number;
}

export interface Sort {
    sorted: boolean;
    unsorted: boolean;
}

export interface Pageable {
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    sort: Sort;
    unpaged: boolean;
}

export interface Sort2 {
    sorted: boolean;
    unsorted: boolean;
}

export class ParkingHttp {
    content: Parking[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort2;
    totalElements: number;
    totalPages: number;
}

export class UsersHttp {
    content: User[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    pageable: Pageable;
    size: number;
    sort: Sort2;
    totalElements: number;
    totalPages: number;
}




