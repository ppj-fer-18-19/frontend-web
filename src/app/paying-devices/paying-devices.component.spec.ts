import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayingDevicesComponent } from './paying-devices.component';

describe('PayingDevicesComponent', () => {
  let component: PayingDevicesComponent;
  let fixture: ComponentFixture<PayingDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayingDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayingDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
