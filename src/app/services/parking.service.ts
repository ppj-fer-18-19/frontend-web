import { Parking } from "../dto/parking.model";
import { HttpClient, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Sensor } from "../dto/sensor.model";
import { ParkingHttp } from "../dto/pageble.model";
import { Subject } from "rxjs";
import { JsonPipe } from "@angular/common";
import { ParkingLotAll, PayingDevice } from "../dto/parkingLotById.model";

@Injectable()
export class ParkingService {

    /** za rute autorizacije prefiks /authorization/
     *  za rute parking-io prefiks /parking/
     */

    private parkings: Parking[] = [];

    parkingsChanged = new Subject<Parking[]>();


    constructor(private httpClient: HttpClient) {
    }


    getParkings() {
        return this.parkings.slice();
    }

    setParkings(parkings: Parking[]) {
        this.parkings = parkings;
        this.parkingsChanged.next(this.parkings.slice());
    }

    getParkingsFromBackend() {
        return this.httpClient.get<ParkingHttp>('/parking/admin/parkingLot/pageable?size=100');
    }

    addParking(parking: Parking) {
        this.parkings.push(parking);
        this.parkingsChanged.next(this.parkings.slice());
        
        const data = {
            "name": parking.name,
            "enabled":`${parking.enabled}`,
            "latitude": parking.latitude,
            "longitude": parking.longitude,
            "zoneId": parking.zoneId
        };

        const req = new HttpRequest('POST', '/parking/admin/parkingLot/register', data , { reportProgress: true })
        return this.httpClient.request(req);
    }

    updateParking(index: number, newParking: Parking) {
        this.parkings[index] = newParking;
        this.parkingsChanged.next(this.parkings.slice());
        const data = {
            "id": newParking.id,
            "name": newParking.name,
            "enabled":`${newParking.enabled}`,
            "latitude": newParking.latitude,
            "longitude": newParking.longitude,
            "zoneId": newParking.zoneId
        };

        const req = new HttpRequest('PUT', '/parking/admin/parkingLot/edit', data , { reportProgress: true })
        return this.httpClient.request(req);
    }

    deleteParking(index: number, parking: Parking) {
        this.parkings.splice(index, 1);
        this.parkingsChanged.next(this.parkings.slice());
        
        const req = new HttpRequest('DELETE', `/parking/admin/parkingLot/delete?id=${parking.id}`, { reportProgress: true }); 
        return this.httpClient.request(req);
    }

    onOffParking(parking: Parking){
        const req = new HttpRequest('PUT', `/parking/admin/parkingLot/changeState?enabled=${!parking.enabled}&id=${parking.id}`, { reportProgress: true }); 
        return this.httpClient.request(req);
    }




    /* --------------------------------------------------------------------------------------------------------------------------- */


    private sensors: Sensor[] = [];

    sensorsChanged = new Subject<Sensor[]>();

    getSensors() {
        return this.sensors.slice();
    }

    setSensors(sensors: Sensor[]) {
        this.sensors = sensors;
        this.sensorsChanged.next(this.sensors.slice());
    }
    
    getsensorsFromBackend(id: string) {
        return this.httpClient.get<ParkingLotAll>(`/parking/admin/parkingLot/${id}`);
    }

    addSensor(sensor: Sensor) {
        this.sensors.push(sensor);
        this.sensorsChanged.next(this.sensors.slice());
        
        const data = {
            "latitude": sensor.latitude,
            "longitude": sensor.longitude,
            "occupied": "false",
            "parkingId": sensor.parkingId
          };

        const req = new HttpRequest('POST', '/parking/admin/sensor/register', data , { reportProgress: true })
        return this.httpClient.request(req);
    }

    updateSensor(index: number, newSensor: Sensor) {
        this.sensors[index] = newSensor;
        this.sensorsChanged.next(this.sensors.slice());
        const data = {
            "id": newSensor.id,
            "latitude": newSensor.latitude,
            "longitude": newSensor.longitude,
            "occupied": "false",
            "parkingId": newSensor.parkingId
          };

        const req = new HttpRequest('PUT', '/parking/admin/sensor/edit', data , { reportProgress: true })
        return this.httpClient.request(req);
    }

    deleteSensor(index: number, sensor: Sensor) {
        this.sensors.splice(index, 1);
        this.sensorsChanged.next(this.sensors.slice());
        
        const req = new HttpRequest('DELETE', `/parking/admin/sensor/delete?id=${sensor.id}`, { reportProgress: true }); 
        return this.httpClient.request(req);
    }

    /*  */


    private payingDevices: PayingDevice[] = [];

    payingDevicesChanged = new Subject<PayingDevice[]>();

    getPayingDevices() {
        return this.payingDevices.slice();
    }

    setPayingDevices(payingDevices: PayingDevice[]) {
        this.payingDevices = payingDevices;
        this.payingDevicesChanged.next(this.payingDevices.slice());
    }
    
    getPayingDevicesFromBackend(id: string) {
        return this.httpClient.get<ParkingLotAll>(`/parking/admin/parkingLot/${id}`);
    }

}