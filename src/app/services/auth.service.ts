import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserA } from '../dto/user';
import { AuthorizationData } from '../dto/authorization.http.model';

@Injectable({ providedIn: 'root' })
export class AuthService {
    private currentUserSubject: BehaviorSubject<AuthorizationData>;
    public currentUser: Observable<AuthorizationData>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<AuthorizationData>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): AuthorizationData {
        return this.currentUserSubject.value;
    }

    login(username: string, password: string) {
        const body = new URLSearchParams();
        body.set('username', username);
        body.set('password', password);
        body.set('grant_type', 'password');

        return this.http.post<any>('/authorization/oauth/token', body.toString(), {
            headers: {
                'Authorization': 'Basic dXNlcjp1c2Vy',
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        })
            .pipe(map(user => {
                // login successful if there's a jwt token in the response
                if (user && user.access_token) {
                /* if (user) { */   
                    console.log(user.access_token);
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                    console.log(user);
                }

                return user;
            }));
    }

    /* login(email: string, password: string){
        let body = new URLSearchParams();
        body.set('username', email);
        body.set('password', password);
        body.set('grant_type', 'password');

        console.log(body.toString());

        const call = this.http.post<Object>(
            '/authorization/oauth/token',
            body.toString(),
            {
                headers: {
                    'Authorization': 'Basic dXNlcjp1c2Vy',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }
        )

        call.subscribe(data => {
            console.log("spremam u local storage");
            localStorage.setItem("jwtToken", JSON.stringify(data));
        });

        return call;
    } */

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}