import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class UserService {

    /** za rute autorizacije prefiks /authorization/
     *  za rute parking-io prefiks /parking/
     */

     private token: any;

    constructor(private httpClient: HttpClient){
    }

    login(email: string, password: string){
        let body = new URLSearchParams();
        body.set('username', email);
        body.set('password', password);
        body.set('grant_type', 'password');

        console.log(body.toString());

        const call = this.httpClient.post<Object>(
            '/authorization/oauth/token',
            body.toString(),
            {
                headers: {
                    'Authorization': 'Basic dXNlcjp1c2Vy',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }
        )

        call.subscribe(data => {
            console.log("spremam u local storage");
            localStorage.setItem("jwtToken", JSON.stringify(data));
        });

        return call;
    }
}