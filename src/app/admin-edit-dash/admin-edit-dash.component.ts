import { Component, OnInit, OnDestroy } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { User } from '../dto/user.model';
import { AdminService } from '../services/admin.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-admin-edit-dash',
  templateUrl: './admin-edit-dash.component.html',
  styleUrls: ['./admin-edit-dash.component.css'],
})
export class AdminEditDashComponent implements OnInit, OnDestroy{
 
  /** Based on the screen size, switch from standard to one column per row */
  /* cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return this.users;
      }

      return this.users;
    })
  ); */

  users: User[]  = [];
  subscription: Subscription;

  constructor(private breakpointObserver: BreakpointObserver,private adminService: AdminService) {}

  ngOnInit(){
    this.subscription = this.adminService.usersChanged
      .subscribe(
        (users: User[]) => {
          this.users = users;
        }
      );
    this.adminService.getUsersFromBackend().subscribe({
      next: (users) => {
        console.log(users.content);
        this.adminService.setUsers(users.content);
      },
      error: (error) => console.log(error)
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onDelete(i) {
    this.adminService.deleteUser(i, this.users[i]).subscribe(
      (response) => {
        console.log(response);
      }
    );
  }

}
