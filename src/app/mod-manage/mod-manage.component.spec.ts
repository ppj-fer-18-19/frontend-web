import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModManageComponent } from './mod-manage.component';

describe('ModManageComponent', () => {
  let component: ModManageComponent;
  let fixture: ComponentFixture<ModManageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModManageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
