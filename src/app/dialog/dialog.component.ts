import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Parking } from '../dto/parking.model';
import { ParkingService } from '../services/parking.service';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})

export class DialogComponent {

  parking: Parking = new Parking(null, null, null, null, null, null, null, null);

  constructor(public dialog: MatDialog, public parkingService: ParkingService) { }

  openDialog() {
    const dialogRef = this.dialog.open(DialogTemplate, {
      width: '250px',
      data: { parking: this.parking }
    });

    dialogRef.afterClosed().subscribe(result => {
      /*       console.log(`Dialog result: ${result}`);
       */      /* this.parking = result; */
      if (!(this.parking.name === null || this.parking.zoneId === null || this.parking.latitude === null
        || this.parking.longitude === null)) {
        console.log(this.parking);
        this.parkingService.addParking(result).subscribe(
          (response) => {
            console.log(response);
            this.parkingService.getParkingsFromBackend().subscribe({
              next: (parking) => {
                console.log(parking.content);
                this.parkingService.setParkings(parking.content);
              },
              error: (error) => console.log(error)
            });
          }
        );
        
      }
      else {
        console.log("Greska u dodavanju!");
      }
      this.parking = new Parking(null, null, null, null, null, null, null, null);
    });
  }
}

@Component({
  selector: 'dialog-template',
  templateUrl: './dialog.html',
})
export class DialogTemplate {
  public get data(): Parking {
    return this._data;
  }
  public set data(value: Parking) {
    this._data = value;
  }
  constructor(
    public dialogRef: MatDialogRef<DialogTemplate>,
    @Inject(MAT_DIALOG_DATA)
    private _data: Parking) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

