import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  //emailForm: FormGroup;
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(private router: Router) {
    // this.emailForm=new FormGroup({
    // email = new FormControl('', [Validators.required, Validators.email]);
    //  })
  }

  onSubmit() {
    console.log(this.email);
  }

  onBack() {
    this.router.navigate(['/signin']);
  }

  ngOnInit() {
  }

}
