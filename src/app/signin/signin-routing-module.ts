import {Routes} from '@angular/router';
import {ForgotPasswordComponent} from './forgot-password.component';

export const singnInRoutes: Routes = [
  {path: 'forgotten', component: ForgotPasswordComponent}
];
