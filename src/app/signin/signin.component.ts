import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  constructor(private userService : UserService) {
  }

   onSubmit(data: NgForm) {
     console.log(data);
     
     this.userService.login(data.form.value.email, data.form.value.password)
        .subscribe(data => console.log(data), err => console.log(err));
   }

  ngOnInit() {
  }

}
