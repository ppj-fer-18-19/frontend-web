import { Component, OnInit, Input } from '@angular/core';
import { Zone } from './zone';

@Component({
  selector: 'app-existing-zone',
  templateUrl: './existing-zone.component.html',
  styleUrls: ['./existing-zone.component.css']
})
export class ExistingZoneComponent implements OnInit {

  @Input() existingZone: Zone;
  @Input() inListID: number;

  constructor() { }

  ngOnInit() {
  }

}
