import { Component, OnInit, OnDestroy } from '@angular/core';
import { Zone } from './zone';
import { ParkingZonesService } from '../services/parking-zones.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-parking-zones',
  templateUrl: './parking-zones.component.html',
  styleUrls: ['./parking-zones.component.css']
})
export class ParkingZonesComponent implements OnInit, OnDestroy {

  constructor(private zonesService: ParkingZonesService) { }

  zones: Zone[];
  subscription: Subscription;

  ngOnInit() {
    this.subscription = this.zonesService.zonesChanged
      .subscribe(
        (zones: Zone[]) => {
          this.zones = zones;
        }
      );
    this.zonesService.getZonesFromBackend().subscribe({
      next: (zones) => {
        console.log(zones);
        this.zonesService.setZones(zones);
      },
      error: (error) => console.log(error)
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
