import { EditParkingZoneComponent } from "../edit-parking-zone/edit-parking-zone.component";


export const ZONES_ROUTES = [
    { path: 'new', component: EditParkingZoneComponent },
    { path: ':id', component: EditParkingZoneComponent }
];