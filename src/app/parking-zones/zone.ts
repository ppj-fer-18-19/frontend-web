export class Zone {
        constructor(public id: number, public name: string, public phone: string, public amount: number,
            public currency: string){}
}
