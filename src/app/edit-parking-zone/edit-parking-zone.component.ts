import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from "@angular/router";
import { Subscription } from "rxjs"
import { ParkingZonesService } from '../services/parking-zones.service';
import { Zone } from '../parking-zones/zone';

@Component({
  selector: 'app-edit-parking-zone',
  templateUrl: './edit-parking-zone.component.html',
  styleUrls: ['./edit-parking-zone.component.css']
})
export class EditParkingZoneComponent implements OnInit, OnDestroy {

  //parkingZones: Zone[]=this.zonesService.getZones();
  parkingZone: Zone;
  parkingIndex: number;
  subscription: Subscription;
  isNew = true;

  /* parkingZone={
     id: 1,
     name: 'zone 1',
     phone: '0918372846',
     amount: 7,
     currency: 'HRK'
   };*/

  parkingZoneForm: FormGroup;


  onSubmit() {

    const newZone: Zone = this.parkingZoneForm.value;
    if (this.isNew) {
      this.zonesService.addZone(newZone).subscribe({
        next: (sensor) => {
          console.log(sensor);
          this.zonesService.getZonesFromBackend().subscribe({
            next: (zones) => {
              console.log(zones);
              this.zonesService.setZones(zones);
            },
            error: (error) => console.log(error)
          });
        },
        error: (error) => console.log(error)
      });;
    }

    else {
      this.zonesService.updateZone(this.parkingIndex, newZone).subscribe({
        next: (sensor) => {
          /* console.log(sensor);
          console.log('kaj'); */
        },
        error: (error) => console.log(error)
      });;
    }
    this.navigateBack();

  }

  onCancel() {
    this.navigateBack();
  }

  onDelete() {
    this.zonesService.deleteZone(this.parkingIndex, this.zonesService.getZone(this.parkingIndex) ).subscribe({
      next: (sensor) => {
        console.log(sensor);
      },
      error: (error) => console.log(error)
    });
    this.navigateBack();
  }

  private navigateBack() {
    this.router.navigate(['/zones']);
  }

  constructor(private router: Router, private route: ActivatedRoute, private zonesService: ParkingZonesService) { }

  initForm() {
    let id;
    let name;
    let phone;
    let amount;
    let currency;

    if (!this.isNew) {
      id = this.parkingZone.id;
      name = this.parkingZone.name;
      phone = this.parkingZone.phone;
      amount = this.parkingZone.amount;
      currency = this.parkingZone.currency;
    }

    this.parkingZoneForm = new FormGroup({
      'id': new FormControl({value: id, disabled:this.isNew}),
      'name': new FormControl(name, Validators.required),
      'phone': new FormControl(phone, Validators.required),
      'amount': new FormControl(amount, Validators.required),
      'currency': new FormControl(currency, Validators.required),

    });


  }

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        if (params.hasOwnProperty('id')) {
          this.isNew = false;
          this.parkingIndex = +params['id'];
          this.parkingZone = this.zonesService.getZone(this.parkingIndex);
        } else {
          this.isNew = true;
          this.parkingZone = null;
        }
        this.initForm();
      }
    );
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
